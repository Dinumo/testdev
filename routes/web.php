<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('dashboard');
})->name('dashboard');



// api test page

Route::get('/api_test', function () {
	return view('api-test');
});

Route::post('/api_test', function (\Illuminate\Http\Request $request) {

	$data = $request->all();

	if($data['ip_addr']) {
		DB::table('api_whitelist')->insert(['ip' => $data['ip_addr'], 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
	}

	return redirect('/api_test');
});

Route::post('/api_test/del', function (\Illuminate\Http\Request $request) {

	$data = $request->all();

	if($data['ip_id']) {
		DB::table('api_whitelist')->where('id', '=', $data['ip_id'])->delete();
	}

	return redirect('/api_test');
});


