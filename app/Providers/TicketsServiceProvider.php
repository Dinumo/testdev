<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\Tickets;

class TicketsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Tickets::class, function(){
            return new Tickets();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
