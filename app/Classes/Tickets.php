<?php

namespace App\Classes;

use App\Models\Tickets as DB_Tickets;
use App\Models\User;
use Carbon\Carbon;


class Tickets {
    
   	static public function createTicket($data) {
   		
   		$lastTicket = DB_Tickets::where('user_id', $data['user_id'])->orderBy('id', 'DESC')->limit(1)->first();

   		if($lastTicket) {
   			if((Carbon::now()->timestamp - $lastTicket->created_at->timestamp) <= 86400) {
   				return [
   					'success' 		=> false,
        			'error' 		=> 'You can create a ticket once a day!'
   				];
   			}
   		}

        $ticket_id = DB_Tickets::create([
        	'user_id'		=> $data['user_id'],
        	'subject'		=> $data['subject'],
        	'message'		=> $data['message'],
        	'username'		=> $data['username'],
        	'user_email'	=> $data['user_email'],
        	'file_link'		=> $data['file_link'],
        ])->id;


        $admin = User::where('role', 'manager')->limit(1)->first();
        if($admin) {
            dispatch(new \App\Jobs\SendEmailJob([
                'subject'       => $data['subject'],
                'msg'           => $data['message'],
                'username'      => $data['username'],
                'user_email'    => $data['user_email'],
                'to_email'      => $admin->email,
                'datetime'      => Carbon::now(),
            ]));
        }


        return [
        	'success' 		=> true,
        	'ticket_id' 	=> $ticket_id
        ];
    }


    static public function getTicket($ticket_id, $user_id = null) {

        if(isset($user_id)) {
            $ticket = DB_Tickets::where('id', $ticket_id)->where('user_id', $user_id)->get();
        } else {
            $ticket = DB_Tickets::where('id', $ticket_id)->get();
        }

        return $ticket;
    }

    static public function getUserTickets($user_id) {
    	return DB_Tickets::where('user_id', $user_id)->get();
    }

    static public function getTickets() {
    	return DB_Tickets::all();
    }

    static public function markTicket($ticket_id) {
    	return DB_Tickets::where('id', $ticket_id)->update(['isMarked' => 1]);
    }

    static public function deleteTickets($ticket_id) {
        return DB_Tickets::destroy($ticket_id);
    }
}