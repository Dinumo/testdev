<?php

namespace App\Http\Livewire\Tickets;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;

use App\Classes\Tickets;


class Create extends Component
{	
    use WithFileUploads;

    public $tickets;

	public $subject;

    public $message;

    public $upload_file;

    protected $rules = [
        'subject' => 'required|min:6',
        'message' => 'required|min:6',
        'upload_file' => 'nullable|image|max:1024',
    ];

    public function render(Tickets $tickets)
    {
        if(Auth::user()->role == 'manager') {
            $this->tickets = $tickets::getTickets();
        } else {
            $this->tickets = $tickets::getUserTickets(Auth::user()->id);
        }

        return view('livewire.tickets.index');
    }

    public function create_ticket(Tickets $tickets) {
        if(Auth::user()->role == 'client') {
            $this->validate();
            
            $this->resetErrorBag();
            $this->resetValidation();

            if($this->upload_file) {
                $file = $this->upload_file->store('public');
            } else {
                $file = null;
            }

            $result = $tickets->createTicket([
                'user_id'       => Auth::user()->id,
                'subject'       => $this->subject,
                'message'       => $this->message,
                'username'      => Auth::user()->name,
                'user_email'    => Auth::user()->email,
                'file_link'     => $file,
            ]);

            if($result['success'] == false) {
                $this->addError('one24', $result['error']);
            }

            $this->reset();
        }
    }

    public function markTicket($ticket_id) {

        if(Auth::user()->role == 'manager') {
            Tickets::markTicket($ticket_id);
        }

        return view('livewire.tickets.index');
    }
}
