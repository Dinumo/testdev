<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use DB;

class CheckIpInWhitelist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $check_ip = DB::table('api_whitelist')->where('ip', $request->ip())->first();

        if(!$check_ip) {
            return response()->json(['success' => false, 'message' => 'Your IP is not in the whitelist'], 404);
        }

        return $next($request);
    }
}
