<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Tickets;
use Validator;
use Auth;

class TicketController extends BaseController
{
    public function index(Tickets $tickets)
    {
        if(Auth::user()->role == 'manager') {
            $tickets = $tickets::getTickets();
        } else {
            $tickets = $tickets::getUserTickets(Auth::user()->id);
        }
        
        return $this->sendResponse($tickets->toArray());
    }


    public function store(Request $request, Tickets $tickets)
    {
    	$input = $request->all();

	 	$validator = Validator::make($input, [
            'subject' => 'required|min:6',
            'message' => 'required|min:6'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error', $validator->errors());   
        }

        $result = $tickets->createTicket([
            'user_id'       => Auth::user()->id,
            'subject'       => $input['subject'],
            'message'       => $input['message'],
            'username'      => Auth::user()->name,
            'user_email'    => Auth::user()->email,
            'file_link'     => '',
        ]);

        if($result['success'] == false) {
        	return $this->sendError($result['error']);
        }

        return $this->sendResponse(['ticket_id' => $result['ticket_id']], 'Ticket created successfully');
    }


    public function show($id, Tickets $tickets)
    {
        if(Auth::user()->role == 'manager') {
            $ticket = $tickets::getTicket($id);
        } else {
            $ticket = $tickets::getTicket($id, Auth::user()->id);
        }

        if($ticket->count() == 0) {
            return $this->sendError('Ticket not found');
        }

        return $this->sendResponse($ticket[0]->toArray());
    }


    public function update($id, Request $request, Tickets $tickets)
    {
    	if(Auth::user()->role != 'manager') {
    		return $this->sendError('Permission denied');
    	}

        $input = $request->all();

        if(isset($input['action']) && $input['action'] == 'mark_ticket') {
            $result = $tickets::markTicket($id);

            if(!$result) {
                return $this->sendError('Ticket not found');
            }
        } else {
            return $this->sendError('Ticket updated failed. Specify the parameter "action"');
        }

        return $this->sendResponse([], 'Ticket updated successfully.');
    }


    public function destroy($id, Tickets $tickets)
    {
    	if(Auth::user()->role != 'manager') {
    		return $this->sendError('Permission denied');
    	}

        $result = $tickets::deleteTickets($id);

        if(!$result) {
        	return $this->sendError('Ticket not found');
        }

        return $this->sendResponse([], 'Ticket deleted successfully');
    }
}
