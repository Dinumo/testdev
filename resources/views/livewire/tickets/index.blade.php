<div>
    @if(Auth::user()->role == 'client')
        <x-jet-form-section submit="create_ticket">
            <x-slot name="title">
                {{ __('Create Ticket') }}
            </x-slot>

            <x-slot name="description">
                {{ __('Create a new ticket. Attention! You can only create a ticket once every 24 hours!') }}
            </x-slot>

            <x-slot name="form">
                <div class="col-span-6 sm:col-span-8">
                    <x-jet-label for="subject" value="{{ __('Subject') }}" />
                    <x-jet-input id="subject" name="subject" type="text" class="mt-1 block w-full" autofocus wire:model.defer="subject"/>
                    <x-jet-input-error for="subject" class="mt-2" />
                </div>

                <div class="col-span-6 sm:col-span-8">
                    <x-jet-label for="message" value="{{ __('Message') }}" />
                    <textarea id="message" name="message" class="mt-1 block w-full form-input h-20" wire:model.defer="message"></textarea>
                    <x-jet-input-error for="message" class="mt-2" />
                </div>

                <div class="col-span-6 sm:col-span-8">

                    <x-jet-input id="upload_file" name="upload_file" type="file" class="hidden upload_file" wire:model.defer="upload_file"/>

    		        <x-jet-label for="file" value="{{ __('File') }}" />

    		        <x-jet-secondary-button class="mt-2 mr-2 uploadFileBtn" type="button" onclick="document.getElementsByClassName('upload_file')[0].click();">
                        @if($upload_file)
                            {{ $upload_file->getClientOriginalName() }}
                        @else
                            {{ __('Attachment a file') }}
                        @endif
                    </x-jet-secondary-button>

                    @error('upload_file') <span class="error" id="upload_file-error">{{ $message }}</span> @enderror

    		        <x-jet-input-error for="file" class="mt-2" />
                </div> 
            </x-slot>

            <x-slot name="actions">
                <x-jet-action-message class="mr-3" on="created">
                    {{ __('Sended.') }}
                </x-jet-action-message>

                <x-jet-button>
                    {{ __('Send') }}
                </x-jet-button>
            </x-slot>
        </x-jet-form-section>

        @error('one24')
            <div class="relative flex items-top justify-center bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0 mt-10" style="color: red;">
                {{ $message }}
            </div>
        @enderror

        <x-jet-section-border />

        @if ($this->tickets->count())
            <div class="mt-10 sm:mt-0">
                <x-jet-action-section>
                    <x-slot name="title">
                        {{ __('Your tickets') }}
                    </x-slot>

                    <x-slot name="description"></x-slot>

                    <x-slot name="content">
                        <div class="space-y-6">
                            @foreach ($this->tickets as $ticket)
                                <div class="flex items-center justify-between my-10">
                                    <div>
                                        {{ $ticket->subject }}
                                    </div>

                                    <div class="flex items-center">
                                        <div class="text-sm text-gray-400 px-4">
                                            {{ $ticket->subject }}
                                        </div>
                                        @if($ticket->file_link)
                                            <x-jet-secondary-button class="mt-2 mr-2" type="button" onclick="window.open('{{ Storage::url($ticket->file_link) }}', '_blank');">
                                                {{ __('file') }}
                                            </x-jet-secondary-button>
                                        @endif
                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </x-slot>
                </x-jet-action-section>
            </div>
        @endif

    @else

        @if ($this->tickets->count())
            <div class="table">
                <table>
                    <thead>
                        <th>ID</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Attachment file</th>
                        <th>Time Created</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach($this->tickets as $ticket)
                            <tr>
                                <td>{{ $ticket->id }}</td>
                                <td>{{ $ticket->subject }}</td>
                                <td>{{ $ticket->message }}</td>
                                <td>{{ $ticket->username }}</td>
                                <td>{{ $ticket->user_email }}</td>
                                <td>
                                    @if($ticket->file_link)
                                        <x-jet-secondary-button class="mt-2 mr-2" type="button" onclick="window.open('{{ Storage::url($ticket->file_link) }}', '_blank');">
                                            {{ __('File') }}
                                        </x-jet-secondary-button>
                                    @endif
                                </td>
                                <td>{{ $ticket->created_at }}</td>
                                <td>
                                    @if($ticket->isMarked == 0)
                                        <x-jet-secondary-button class="mt-2 mr-2" type="button" wire:click="markTicket({{ $ticket->id }})">
                                            {{ __('Mark') }}
                                        </x-jet-secondary-button>
                                    @else
                                        Marked                               
                                    @endif                               
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="relative flex items-top justify-center bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0 mt-10" style="color: red;">No tickets :(</div>
        @endif
    @endif

</div>
