<?php 
	$ips = DB::table('api_whitelist')->get();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Api test</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container-fluid">
		<div class="row" style="margin-bottom: 20px;margin-top: 20px;">
			<div class="col-sm-3">
				<h5>Requests</h5>
				<div class="form-group">
					<input type="text" name="api_key" class="form-control" placeholder="Enter API Key...">
				</div>
				<div class="form-group">
					<input type="text" name="ticket_id" class="form-control" placeholder="Enter Ticket id...">
				</div>
				<div class="form-group">
					<input type="text" name="subject" class="form-control" placeholder="Enter subject...">
				</div>
				<div class="form-group">
					<input type="text" name="message" class="form-control" placeholder="Enter message...">
				</div>
				<div class="form-group">
					<input type="text" name="action" class="form-control" placeholder="Enter action for update...">
					<span>Use <b>"mark_ticket"</b> action. Available only to the Manager</span>
				</div>
			</div>
			<div class="col-sm-3 offset-sm-2">
				<h5>Whitelist</h5>
				<div class="form-group">
					<form action="/api_test" method="POST">
						{{ csrf_field() }}
						<input type="text" name="ip_addr" class="form-control" placeholder="Enter IP address..." style="display: inline-block;width: 80%;float: left;">
						<button class="btn btn-success" style="display: inline-block;">Add</button>
					</form>
				</div>
				<div class="form-group">
					@if($ips->count())
						@foreach($ips as $ip)
							<form action="/api_test/del" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="ip_id" value="{{ $ip->id }}">
								<input disabled type="text" name="ip" class="form-control" value="{{ $ip->ip }}" style="display: inline-block;width: 80%;float: left;">
								<button class="btn btn-danger" style="display: inline-block;">Del</button>
							</form>
						@endforeach
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<button id="get" class="btn btn-success">GET</button>
				<button id="post" class="btn btn-warning">POST</button>
				<button id="patch" class="btn btn-info">PATCH</button>
				<button id="delete" class="btn btn-danger">DELETE</button>
			</div>
		</div>

		<div class="row" style="margin: 100px 0 0;">
			<div class="col-sm-12">
				<h5>Results</h5>
				<div class="form-group">
					<div class="workspace" style="border: solid 1px #dadada;border-radius: 5px;padding: 5px 15px;">
						<p class="httpcode" style="font-weight: bold;"></p>
						<p class="result"></p>
					</div>
				</div>
			</div>
		</div>

		<div class="row" style="margin: 50px 0 0;">
			<div class="col-sm-12">
				<h5>Описание API</h5>
				
				<br>

				<div>
					<p><b>GET</b> - получение данных о тикетах</p>
					<p>Обязательные параметры: api_key.</p>
					<p>Необязательные параметры: ticket_id.</p>
					<p>
						<div class="alert alert-info">Если ticket_id не указан, то обычный пользователь получает в ответе все имеющиеся тикеты, которые относятся к данному пользователю.<br>
						Если ticket_id не указан, то менеджер получает в ответе список тикетов всех пользователей.</div>
					</p>
					<p>
						<div class="alert alert-success">
							<p><b>Response 200</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"true","data":[{"id":"14    // идентификатор тикета","user_id":"2    // идентификатор пользователя","subject":"My ticket subject    // тема тикета","message":"My ticket message    // сообщение тикета","username":"Jacob    // имя пользователя","user_email":"jacob@mail.ru    // email пользователя","file_link":"null    // путь к прикрепленному файлу","isMarked":"1    // отметка менеджера","created_at":"2020-10-08T10:35:21.000000Z    // время создания","updated_at":"2020-10-12T13:51:35.000000Z    // время обновления"},{"id":16,"user_id":2,"subject":"My ticket subject 2","message":"My ticket message 2","username":"Jacob","user_email":"jacob@mail.ru","file_link":"","isMarked":0,"created_at":"2020-10-13T11:50:41.000000Z","updated_at":"2020-10-13T11:50:41.000000Z"}],"message":""}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>data</b> - массив получаемых данных. Если ticket_id не указан, то возвращается массив данных, как в примере выше. Если же ticket_id указан, то ячейка <b>data</b> будет содержать не массив, а ячейки данных тикета.<br>
								<b>message</b> - сообщение системы<br>
							</p>
						</div>
					</p>
					<p>
						<div class="alert alert-danger">
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Ticket not found"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что тикет не найден.<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Your IP is not in the whitelist"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что ip адрес, с которого делается запрос не числится в белом списке.<br>
							</p>
							<hr>
							<p><b>Response 401</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"message":"Unauthenticated."}', true)); ?></p>
							<p>
								<b>message</b> - сообщение системы о том, что авторизация не прошла (вероятно не верный api_key)<br>
							</p>
						</div>
					</p>
				</div>

				<br><hr><br>

				<div>
					<p><b>POST</b> - создание тикета</p>
					<p>Обязательные параметры: api_key, subject, message</p>
					<p>
						<div class="alert alert-success">
							<p><b>Response 200</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"true","data":{"ticket_id":17},"message":"Ticket created successfully"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>data</b> - массив, в котором находится единственная ячейка под название ticket_id, обозначающая идентификатор созданного тикета.<br>
								<b>message</b> - сообщение системы о успешном создании тикета<br>
							</p>
						</div>
					</p>
					<p>
						<div class="alert alert-danger">
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Validation Error","data":{"subject":["The subject field is required."],"message":["The message field is required."]}}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, входящие параметры не прошли валидацию.<br>
								<b>data</b> - массив входящих параметров, которые не прошли валидацию.<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"You can create a ticket once a day!"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что тикет можно создавать один раз в 24 часа.<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Your IP is not in the whitelist"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что ip адрес, с которого делается запрос не числится в белом списке.<br>
							</p>
							<hr>
							<p><b>Response 401</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"message":"Unauthenticated."}', true)); ?></p>
							<p>
								<b>message</b> - сообщение системы о том, что авторизация не прошла (вероятно не верный api_key)<br>
							</p>
						</div>
					</p>
				</div>

				<br><hr><br>

				<div>
					<p><b>PATCH</b> - обновление тикета</p>
					<p>Обязательные параметры: api_key, ticket_id, action (существует единственное значение для данного параметра - <b>mark_ticket</b>)</p>
					<p>
						<div class="alert alert-success">
							<p><b>Response 200</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"true","data":[],"message":"Ticket updated successfully."}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>data</b> - массив данных.<br>
								<b>message</b> - сообщение системы об успешном изменении тикета<br>
							</p>
						</div>
					</p>
					<p>
						<div class="alert alert-danger">
							<p><b>Response 405</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"message":"The PATCH method is not supported for this route. Supported methods: GET, HEAD, POST."}', true)); ?></p>
							<p>
								<b>message</b> - сообщение системы о том, что данный метод недоступен. (Нужно указать ticket_id)<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Permission denied"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что нет доступа к данной функции. (Доступно только менеджеру)<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Ticket not found"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что указанный тикет не найден.<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Ticket updated failed. Specify the parameter \"action\""}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что произошла ошибка - не указан параметр action.<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Your IP is not in the whitelist"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что ip адрес, с которого делается запрос не числится в белом списке.<br>
							</p>
							<hr>
							<p><b>Response 401</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"message":"Unauthenticated."}', true)); ?></p>
							<p>
								<b>message</b> - сообщение системы о том, что авторизация не прошла (вероятно не верный api_key)<br>
							</p>
						</div>
					</p>
				</div>

				<br><hr><br>

				<div>
					<p><b>DELETE</b> - удалание тикета</p>
					<p>Обязательные параметры: api_key, ticket_id</p>
					<p>
						<div class="alert alert-success">
							<p><b>Response 200</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"true","data":[],"message":"Ticket deleted successfully."}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>data</b> - массив данных.<br>
								<b>message</b> - сообщение системы об успешном удалении тикета<br>
							</p>
						</div>
					</p>
					<p>
						<div class="alert alert-danger">
							<p><b>Response 405</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"message":"The PATCH method is not supported for this route. Supported methods: GET, HEAD, POST."}', true)); ?></p>
							<p>
								<b>message</b> - сообщение системы о том, что данный метод недоступен. (Нужно указать ticket_id)<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Permission denied"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что нет доступа к данной функции. (Доступно только менеджеру)<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Ticket not found"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что указанный тикет не найден.<br>
							</p>
							<hr>
							<p><b>Response 404</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"success":"false","message":"Your IP is not in the whitelist"}', true)); ?></p>
							<p>
								<b>success</b> - true/false (1/0). Статус выполнения запроса, если true, то запрос выполенен успешно, если false, то возникла ошибка. (Про ошибки читать ниже)<br>
								<b>message</b> - сообщение системы о том, что ip адрес, с которого делается запрос не числится в белом списке.<br>
							</p>
							<hr>
							<p><b>Response 401</b></p>
							<p style="white-space: pre-wrap;"><?php print_r(json_decode('{"message":"Unauthenticated."}', true)); ?></p>
							<p>
								<b>message</b> - сообщение системы о том, что авторизация не прошла (вероятно не верный api_key)<br>
							</p>
						</div>
					</p>
				</div>

			</div>
		</div>
	</div>

	<style type="text/css">
		.alert hr {
			margin: 40px 0;
		}
	</style>

	<script>
		$(document).ready(function() {

			// jd3qx4ZRkWsAil7H0AlzXGpGJ6xPt6SrzqTDlu3L
			// vm48V21zB3Q916Mnfe5GaZK7KeHDDPnu35j3exfJ

			function getApiKey() {
				return $(document).find('input[name="api_key"]').val();
			}

			function getTicketId() {
				return $(document).find('input[name="ticket_id"]').val();
			}

			function writeResult(code, result) {
				$(document).find('.workspace .httpcode').text('HTTP STATUS: ' + code);

				var result = JSON.stringify(result);

				$(document).find('.workspace .result').text(result);
			}

			$(document).on('click', '#get', function() {

				var ticket_id = getTicketId();

				if(ticket_id != '') {
					ticket_id = '/' + ticket_id;
				}

				$.ajax({
					url: "/api/tickets" + ticket_id,
					method: "GET",
					headers: {
						'Authorization': 'Bearer ' + getApiKey(),
					},
					dataType: 'json',
					success: function(json) {
						console.log(json);
						writeResult(200, json);
					},
					error: function(a,b,c) {
						console.log(a.status);
						console.log(a.responseJSON);

						writeResult(a.status, a.responseJSON);
					}
				});
			});

			$(document).on('click', '#post', function() {
				$.ajax({
					url: "/api/tickets",
					method: "POST",
					data: {
						subject: $(document).find('input[name="subject"]').val(),
						message: $(document).find('input[name="message"]').val(),
					},
					headers: {
						'Authorization': 'Bearer ' + getApiKey(),
					},
					dataType: 'json',
					success: function(json) {
						console.log(json);
						writeResult(200, json);
					},
					error: function(a,b,c) {
						console.log(a.status);
						console.log(a.responseJSON);

						writeResult(a.status, a.responseJSON);
					}
				});
			});


			$(document).on('click', '#patch', function() {

				var ticket_id = getTicketId();

				if(ticket_id != '') {
					ticket_id = '/' + ticket_id;
				}

				$.ajax({
					url: "/api/tickets" + ticket_id,
					method: "PATCH",
					data: {
						action: $(document).find('input[name="action"]').val(),
					},
					headers: {
						'Authorization': 'Bearer ' + getApiKey(),
					},
					dataType: 'json',
					success: function(json) {
						console.log(json);
						writeResult(200, json);
					},
					error: function(a,b,c) {
						console.log(a.status);
						console.log(a.responseJSON);

						writeResult(a.status, a.responseJSON);
					}
				});
			});


			$(document).on('click', '#delete', function() {

				var ticket_id = getTicketId();

				if(ticket_id != '') {
					ticket_id = '/' + ticket_id;
				}

				$.ajax({
					url: "/api/tickets" + ticket_id,
					method: "DELETE",
					headers: {
						'Authorization': 'Bearer ' + getApiKey(),
					},
					dataType: 'json',
					success: function(json) {
						console.log(json);
						writeResult(200, json);
					},
					error: function(a,b,c) {
						console.log(a.status);
						console.log(a.responseJSON);

						writeResult(a.status, a.responseJSON);
					}
				});
			});

			
		});
	</script>

</body>
</html>