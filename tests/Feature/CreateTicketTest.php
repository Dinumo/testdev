<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Livewire;
use Illuminate\Support\Str;

class CreateTicketTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateTicket()
    {   
        $users = [];

        for($i = 0; $i <= 10; $i++) {
            $users[] = User::factory()->create(['role' => 'client']);
        }

        foreach ($users as $user) {
            Livewire::actingAs($user)
                ->test(\App\Http\Livewire\Tickets\Create::class)
                ->set('subject', 'test subject')
                ->set('message', 'test message')
                ->call('create_ticket');
        }

    }
}
